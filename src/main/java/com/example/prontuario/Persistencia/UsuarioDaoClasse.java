package com.example.prontuario.Persistencia;

import com.example.prontuario.Modelo.Usuario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioDaoClasse implements UsuarioDaoInterface, AutoCloseable {
    private Connection con;
    public UsuarioDaoClasse() throws ErroDAO {
        con = FabricaConexao.pegaConexao();
    }

    @Override
    public void inserirPaciente(Usuario u) throws ErroDAO {
        String sql = "insert into usuario(login, senha, tipo, nome, cpf, sexo, data_nascimento, nomeMae, uf, cidade, bairro, endereco) values(?, ?,?,?,?,?,?,?,?,?,?,?)";
        try (PreparedStatement pstm = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pstm.setString(1, u.getLogin());
            pstm.setString(2, u.getSenha());
            pstm.setString(3, u.getTipo());
            pstm.setString(4, u.getNome());
            pstm.setString(5, u.getCpf());
            pstm.setString(6, u.getSexo());
            pstm.setString(7, u.getDataNascimento());
            pstm.setString(8, u.getNome_mae());
            pstm.setString(9, u.getUf());
            pstm.setString(10, u.getCidade());
            pstm.setString(11, u.getBairro());
            pstm.setString(12, u.getEndereco());

            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                u.setCodigo(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public void inserirMedico(Usuario u) throws ErroDAO {
        String sql = "insert into usuario(tipo, nome, cpf, login, senha, especializacao) values(?,?,?,?,?,?)";
        try (PreparedStatement pstm = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pstm.setString(1, u.getTipo());
            pstm.setString(2, u.getNome());
            pstm.setString(3, u.getCpf());
            pstm.setString(4, u.getLogin());
            pstm.setString(5, u.getSenha());
            pstm.setString(6, u.getEspecializacao()
            );
            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                u.setCodigo(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public void editarPaciente(Usuario u) throws ErroDAO{
        String sql = "UPDATE usuario SET endereco = ? , senha = ? WHERE codigo = ? ";
        try (PreparedStatement pstm = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pstm.setString(1, u.getEndereco());
            pstm.setString(2, u.getSenha());
            pstm.setInt(3, u.getCodigo());
            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                u.setCodigo(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public void editarPacientePorMedico(Usuario u) throws ErroDAO{
        String sql = "UPDATE usuario SET nome = ?, login = ?, cpf = ?, nomeMae = ?, uf = ?, cidade = ?, bairro = ?, endereco = ?  WHERE codigo = ?";
        try (PreparedStatement pstm = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {

            pstm.setString(1, u.getNome());
            pstm.setString(2, u.getLogin());
            pstm.setString(3, u.getCpf());
            pstm.setString(4, u.getNome_mae());
            pstm.setString(5, u.getUf());
            pstm.setString(6, u.getCidade());
            pstm.setString(7, u.getBairro());
            pstm.setString(8, u.getEndereco());
            pstm.setInt(9, u.getCodigo());

            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                u.setCodigo(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    public void editarMedico(Usuario u) throws ErroDAO{
        String sql = "UPDATE usuario SET nome = ? , senha = ?, cpf = ?, especializacao = ? WHERE codigo = ? ";
        try (PreparedStatement pstm = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pstm.setString(1, u.getNome());
            pstm.setString(2, u.getSenha());
            pstm.setString(3, u.getCpf());
            pstm.setString(4, u.getEspecializacao());
            pstm.setInt(5, u.getCodigo());
            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                u.setCodigo(rs.getInt(1));
            }
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public Usuario buscar(String login, String senha) throws ErroDAO {
        String sql="select * from usuario where login=? and senha=?";
        try (PreparedStatement pstm=con.prepareStatement(sql)){
            pstm.setString(1,login);
            pstm.setString(2,senha);
            ResultSet rs=pstm.executeQuery();
            if(rs.next())
            {
                Usuario u=new Usuario();
                u.setCodigo(rs.getInt("codigo"));
                u.setLogin(rs.getString("login"));
                u.setSenha(rs.getString("senha"));
                u.setNome(rs.getString("nome"));
                u.setTipo(rs.getString("tipo"));
                u.setEndereco(rs.getString("endereco"));
                u.setCpf(rs.getString("cpf"));
                u.setEspecializacao(rs.getString("especializacao"));

                return u;
            }
            return null;
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public Usuario buscaPacientePorCodigo(String codigo) throws ErroDAO {
        String sql="select * from usuario where codigo = ?";
        try (PreparedStatement pstm=con.prepareStatement(sql)){
            pstm.setString(1,codigo);
            ResultSet rs=pstm.executeQuery();
            if(rs.next())
            {
                Usuario u=new Usuario();
                u.setCodigo(rs.getInt("codigo"));
                u.setLogin(rs.getString("login"));
                u.setSenha(rs.getString("senha"));
                u.setNome(rs.getString("nome"));
                u.setTipo(rs.getString("tipo"));
                u.setEndereco(rs.getString("endereco"));
                u.setCpf(rs.getString("cpf"));
                u.setNome_mae(rs.getString("nomeMae"));
                u.setUf(rs.getString("uf"));
                u.setCidade(rs.getString("cidade"));
                u.setEndereco(rs.getString("endereco"));
                u.setBairro(rs.getString("bairro"));
                return u;
            }
            return null;
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public Usuario buscar(Usuario u) throws ErroDAO {
        return null;
    }

    @Override
    public boolean bucarPorCPF(String cpf) throws SQLException {
        boolean pacienteExiste = false;
        String sql = "SELECT COUNT(*) AS count FROM usuario WHERE cpf = ?";
        try (PreparedStatement pstm = con.prepareStatement(sql)) {
            pstm.setString(1, cpf);
            ResultSet rs = pstm.executeQuery();

            if (rs.next()) {
                int count = rs.getInt("count");
                pacienteExiste = count > 0;
            }

            return pacienteExiste;
        }
    }

    public String buscaPaciente(String cpf) throws SQLException {
        String sql = "SELECT codigo FROM usuario WHERE cpf = ?";
        try (PreparedStatement pstm = con.prepareStatement(sql)) {
            pstm.setString(1, cpf);
            ResultSet rs = pstm.executeQuery();
            if(rs.next()){
                String codigo = rs.getString("codigo");
                return codigo;
            }else {
                return null;
            }

        }

    }

    public String buscaTipo(String login) throws SQLException{
        String sql = "SELECT tipo FROM usuario WHERE login = ?";
        try (PreparedStatement pstm = con.prepareStatement(sql)) {
            pstm.setString(1, login);
            ResultSet rs = pstm.executeQuery();
            String tipo = "";
            if(rs.next()){
                tipo = rs.getString("tipo");
                return tipo;
            }else {
                return null;
            }
        }
    }

    @Override
    public ArrayList<Usuario> buscaTodosPacientes(String nome) throws ErroDAO {

        String nomeTratadoParaLike = "%"+nome+"%";

        String sql="SELECT usuario.codigo, usuario.cpf, usuario.nome, DATE_FORMAT(STR_TO_DATE(data_nascimento, '%Y-%m-%d'), '%d/%m/%Y') as data_nascimento FROM usuario WHERE usuario.tipo = 'p' and usuario.nome LIKE ?";
        try (PreparedStatement pstm=con.prepareStatement(sql)){
            pstm.setString(1,nomeTratadoParaLike);
            ResultSet rs=pstm.executeQuery();
            ArrayList<Usuario> Usuarios=new ArrayList<>();
            while (rs.next())
            {
                Usuario u = new Usuario();
                u.setCodigo(rs.getInt("codigo"));
                u.setNome(rs.getString("nome"));
                u.setCpf(rs.getString("cpf"));
                u.setDataNascimento(rs.getString("data_nascimento"));
                Usuarios.add(u);
            }
            return Usuarios;
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
        public void close() throws SQLException {
        con.close();
    }
}
