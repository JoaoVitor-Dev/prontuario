package com.example.prontuario.Persistencia;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FabricaConexao {
    public static Connection pegaConexao() throws ErroDAO{
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection
                    ("jdbc:mysql://localhost:3306/prontuario?useSSL=false","root","root");
        } catch (ClassNotFoundException | SQLException e) {
            throw new ErroDAO(e);
        }
    }
    public static void main(String[] args)  {
        try {
            FabricaConexao.pegaConexao();
            System.out.println("ok");
        } catch (ErroDAO e) {
            throw new RuntimeException(e);
        }
    }
}
