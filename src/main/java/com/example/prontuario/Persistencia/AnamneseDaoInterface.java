package com.example.prontuario.Persistencia;

import com.example.prontuario.Modelo.Anamnese;

import java.util.ArrayList;

public interface AnamneseDaoInterface {


    void inserirAnamnese(Anamnese a) throws ErroDAO;

    Anamnese buscar(Anamnese u) throws ErroDAO;


    ArrayList<Anamnese> buscaTodasAnamenses(String codigo) throws ErroDAO;


    ArrayList<Anamnese> buscaAnamnesePorData(String data, int codigo) throws ErroDAO;

    void close() throws Exception;
}
