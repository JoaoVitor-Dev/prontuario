package com.example.prontuario.Persistencia;

import com.example.prontuario.Modelo.Anamnese;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AnamneseDaoClasse implements AnamneseDaoInterface, AutoCloseable {
    private Connection con;
    public AnamneseDaoClasse() throws ErroDAO {
        con = FabricaConexao.pegaConexao();
    }
    @Override
    public void inserirAnamnese(Anamnese a) throws ErroDAO {
        String sql = "INSERT INTO anamnese(codigo_paciente, codigo_medico, exame_fisico, laudos, hipoteses, diagnostico, tratamento, data)\n" +
                "values(?,?,?,?,?,?,?,?);";
        try (PreparedStatement pstm = con.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS)) {
            pstm.setInt(1, a.getCodigo_paciente());
            pstm.setInt(2, a.getCodigo_medico());
            pstm.setString(3, a.getExame_fisico());
            pstm.setString(4, a.getLaudos());
            pstm.setString(5, a.getHipoteses());
            pstm.setString(6, a.getDiagnostico());
            pstm.setString(7, a.getTratamento());
            pstm.setString(8, a.getData());
            pstm.executeUpdate();
            ResultSet rs = pstm.getGeneratedKeys();
            if (rs.next()) {
                a.setCodigo(rs.getInt(1));
            }

        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }


    @Override
    public Anamnese buscar(Anamnese u) throws ErroDAO {
        return null;
    }


    @Override
    public ArrayList<Anamnese> buscaTodasAnamenses(String codigo) throws ErroDAO {
        String sql="SELECT anamnese.codigo, codigo_paciente, codigo_medico, exame_fisico, laudos, hipoteses, diagnostico, tratamento, usuario.nome, DATE_FORMAT(STR_TO_DATE(data, '%Y-%m-%d'), '%d/%m/%Y') as data FROM anamnese Inner Join usuario on usuario.codigo = anamnese.codigo_medico where anamnese.codigo_paciente = ?";
        try (PreparedStatement pstm=con.prepareStatement(sql)){
            pstm.setString(1,codigo);
            ResultSet rs=pstm.executeQuery();
            ArrayList<Anamnese> Anamneses=new ArrayList<>();
            while (rs.next())
            {
                Anamnese a=new Anamnese();
                a.setCodigo_paciente(rs.getInt("codigo_paciente"));
                a.setCodigo(rs.getInt("codigo"));
                a.setExame_fisico(rs.getString("exame_fisico"));
                a.setLaudos(rs.getString("laudos"));
                a.setHipoteses(rs.getString("hipoteses"));
                a.setDiagnostico(rs.getString("diagnostico"));
                a.setTratamento(rs.getString("tratamento"));
                a.setData(rs.getString("data"));
                a.setMedico_responsavel(rs.getString("nome"));
                Anamneses.add(a);
            }
            return Anamneses;
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }

    @Override
    public ArrayList<Anamnese> buscaAnamnesePorData(String data, int codigo) throws ErroDAO {
        String sql="SELECT anamnese.codigo, codigo_paciente, codigo_medico, exame_fisico, laudos, hipoteses, diagnostico, tratamento, usuario.nome, DATE_FORMAT(STR_TO_DATE(data, '%Y-%m-%d'), '%d/%m/%Y') as data FROM anamnese inner join usuario on usuario.codigo = anamnese.codigo_medico where data=? and codigo_paciente = ?";
        try (PreparedStatement pstm=con.prepareStatement(sql)){
            pstm.setString(1,data);
            pstm.setInt(2,codigo);
            ResultSet rs=pstm.executeQuery();
            ArrayList<Anamnese> Anamneses=new ArrayList<>();
            while (rs.next())
            {
                Anamnese a=new Anamnese();
                a.setCodigo(rs.getInt("codigo"));
                a.setExame_fisico(rs.getString("exame_fisico"));
                a.setLaudos(rs.getString("laudos"));
                a.setHipoteses(rs.getString("hipoteses"));
                a.setDiagnostico(rs.getString("diagnostico"));
                a.setTratamento(rs.getString("tratamento"));
                a.setData(rs.getString("data"));
                a.setMedico_responsavel(rs.getString("nome"));
                Anamneses.add(a);
            }
            return Anamneses;
        } catch (SQLException e) {
            throw new ErroDAO(e);
        }
    }




    @Override
    public void close() throws Exception {
        con.close();
    }
}
