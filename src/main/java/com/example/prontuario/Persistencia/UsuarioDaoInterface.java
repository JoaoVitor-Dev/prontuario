package com.example.prontuario.Persistencia;

import com.example.prontuario.Modelo.Anamnese;
import com.example.prontuario.Modelo.Usuario;

import java.sql.SQLException;
import java.util.ArrayList;

public interface UsuarioDaoInterface {
    void inserirPaciente(Usuario u) throws ErroDAO;
    void inserirMedico(Usuario u) throws ErroDAO;

    void editarPaciente(Usuario u) throws ErroDAO;

    void editarPacientePorMedico(Usuario u) throws ErroDAO;

    Usuario buscar(String login, String senha) throws ErroDAO;

    Usuario buscaPacientePorCodigo(String codigo) throws ErroDAO;

    Usuario buscar(Usuario u) throws ErroDAO;

    boolean bucarPorCPF(String cpf) throws SQLException;

    ArrayList<Usuario> buscaTodosPacientes(String nome) throws ErroDAO;
}
