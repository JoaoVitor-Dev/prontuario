package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "EditarMedico", value = "/EditarMedico")
public class EditarMedico extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession sessao=request.getSession(false);

        Usuario usuario=(Usuario)sessao.getAttribute("usuario");

        String nome = request.getParameter("nome");
        String senha = request.getParameter("senha");
        String cpf = request.getParameter("cpf");
        String especializacao = request.getParameter("especializacao");

        try {
            UsuarioDaoClasse dao = new UsuarioDaoClasse();

            usuario.setNome(nome);
            usuario.setSenha(senha);
            usuario.setCpf(cpf);
            usuario.setEspecializacao(especializacao);
            dao.editarMedico(usuario);

            response.sendRedirect("dados.jsp?mensagem="+ URLEncoder.encode("Usuario alterado"));

        } catch (ErroDAO e) {
            throw new RuntimeException(e);
        }
    }
}
