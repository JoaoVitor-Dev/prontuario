package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Anamnese;
import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.AnamneseDaoClasse;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;

@WebServlet(name = "CadastrarAnamnese", value = "/CadastrarAnamnese")
public class CadastrarAnamnese extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cpf=request.getParameter("cpf");
        String exameFisico=request.getParameter("exame-fisico");
        String examesComplementares=request.getParameter("exames-complementares");
        String hipotesesDiagnosticas=request.getParameter("hipoteses-diagnosticas");
        String laudos=request.getParameter("laudos");
        String diagnosticoDefinitivo=request.getParameter("diagnostico-definitivo");
        String tratamentoEfetuado=request.getParameter("tratamento-efetuado");
        String dataHora=request.getParameter("data-hora");
        int medicoResponsavel= Integer.parseInt(request.getParameter("medico-responsavel"));


        if(cpf!=null && exameFisico !=null && examesComplementares!=null && hipotesesDiagnosticas !=null && diagnosticoDefinitivo!= null && tratamentoEfetuado != null)
        {
            UsuarioDaoClasse usuarioDao = null;
            try {
                usuarioDao = new UsuarioDaoClasse();

                boolean pacienteExiste = usuarioDao.bucarPorCPF(cpf);

                if(pacienteExiste == true) {

                    int codigo = Integer.parseInt(usuarioDao.buscaPaciente(cpf));

                    try(AnamneseDaoClasse dao = new AnamneseDaoClasse())
                    {
                        Anamnese a = new Anamnese(0, codigo, medicoResponsavel, exameFisico, laudos, hipotesesDiagnosticas,hipotesesDiagnosticas, tratamentoEfetuado, dataHora);
                        dao.inserirAnamnese(a);
                        response.sendRedirect("anamnese.jsp?prontuario="+ URLEncoder.encode("sim"));
                    } catch (ErroDAO e) {
                        response.sendRedirect("anamnese.jsp?mensagem="+URLEncoder.encode("Erro ao tentar cadastrar"+e));
                    } catch (Exception e) {
                        response.sendRedirect("anamnese.jsp?mensagem="+URLEncoder.encode("Erro ao tentar fechar a conexão"));
                    }
                }else{
                    response.sendRedirect("anamnese.jsp?mensagem="+ URLEncoder.encode("Paciente não encontrado!"));
                }
            } catch (ErroDAO e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        else {
            response.sendRedirect("anamnese.jsp?mensagem="+URLEncoder.encode("Está faltando dados"));
        }
    }

    }

