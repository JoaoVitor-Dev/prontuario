package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;

@WebServlet(name = "EditarPacientePorMedico", value = "/EditarPacientePorMedico")
public class EditarPacientePorMedico extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String codigo = request.getParameter("codigo");

        try {
            UsuarioDaoClasse DAOusuario = new UsuarioDaoClasse();

            Usuario paciente = DAOusuario.buscaPacientePorCodigo(codigo);

            request.setAttribute("paciente", paciente);

            request.getRequestDispatcher("/WEB-INF/EditaPaciente.jsp").forward(request,response);

        } catch (ErroDAO e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int codigo = Integer.parseInt(request.getParameter("codigo"));
        String nome=request.getParameter("nome");
        String login=request.getParameter("login");
        String senha=request.getParameter("senha");
        String nome_mae=request.getParameter("nomeMae");
        String uf=request.getParameter("uf");
        String cidade=request.getParameter("cidade");
        String bairro=request.getParameter("bairro");
        String endereco=request.getParameter("endereco");
        String cpf=request.getParameter("cpf");
        String sexo = request.getParameter("sexo");
        String dataNascimento = request.getParameter("data-nascimento");

        if(nome != null && login != null && senha != null && nome_mae != null && uf != null && cidade != null && bairro != null && endereco != null && cpf != null && codigo != 0 ){

            try {
                UsuarioDaoClasse dao = new UsuarioDaoClasse();

                Usuario usuario = new Usuario(codigo, "p", login, cpf,senha, nome, sexo, dataNascimento, nome_mae, uf, cidade, bairro, endereco);

                usuario.setCodigo(codigo);
                usuario.setNome(nome);
                usuario.setLogin(login);
                usuario.setCpf(cpf);
                usuario.setSenha(senha);
                usuario.setNome_mae(nome_mae);
                usuario.setUf(uf);
                usuario.setCidade(cidade);
                usuario.setBairro(bairro);
                usuario.setEndereco(endereco);

                dao.editarPacientePorMedico(usuario);

                response.sendRedirect("home.jsp?mensagem="+ URLEncoder.encode("Paciente alterado com sucesso!"));

            } catch (ErroDAO e) {
                throw new RuntimeException(e);
            }
        }else {
            response.sendRedirect("home.jsp?mensagem="+ URLEncoder.encode("campos nao podem ser vazios alterado"));
        }



    }
}
