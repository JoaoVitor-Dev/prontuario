package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "EditarPaciente", value = "/EditarPaciente")
public class EditarPaciente extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession sessao=request.getSession(false);

        Usuario usuario=(Usuario)sessao.getAttribute("usuario");

        String endereco = request.getParameter("endereco");
        String senha = request.getParameter("senha");

        try {
            UsuarioDaoClasse dao = new UsuarioDaoClasse();

            usuario.setEndereco(endereco);
            usuario.setSenha(senha);

            dao.editarPaciente(usuario);

            response.sendRedirect("dados.jsp?mensagem="+URLEncoder.encode("Usuario alterado"));

        } catch (ErroDAO e) {
            throw new RuntimeException(e);
        }

    }

}


