package com.example.prontuario.Controle;

import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;

@WebServlet(name = "Sair", value = "/Sair")
public class Sair extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession sessao=request.getSession(false);
        if(sessao!=null)
        {
            sessao.removeAttribute("usuario");
            sessao.removeAttribute("paciente");
            sessao.removeAttribute("anamneses");
            sessao.removeAttribute("pacientes");
        }
        response.sendRedirect("index.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
