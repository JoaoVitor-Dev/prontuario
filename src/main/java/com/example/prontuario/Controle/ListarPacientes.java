package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Anamnese;
import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.AnamneseDaoClasse;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

@WebServlet(name = "ListarPacientes", value = "/ListarPacientes")
public class ListarPacientes extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession sessao=request.getSession(false);

        Usuario usuario=(Usuario)sessao.getAttribute("usuario");

        String nome = request.getParameter("nome");

        if(nome != null && nome != ""){
            try {
                UsuarioDaoClasse DAOusuario = new UsuarioDaoClasse();

                List<Usuario> pacientesList = DAOusuario.buscaTodosPacientes(nome);

                request.setAttribute("pacientes", pacientesList);

                request.getRequestDispatcher("/WEB-INF/ListagemPacientes.jsp").forward(request,response);


            } catch (ErroDAO e) {
                throw new RuntimeException(e);
            }
        }else {
            response.sendRedirect("pesquisapacientes.jsp?mensagem="+ URLEncoder.encode("Nome não informado"));
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
