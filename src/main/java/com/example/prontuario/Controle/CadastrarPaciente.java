package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.SQLException;

@WebServlet(name = "CadastrarPaciente", value = "/CadastrarPaciente")
public class CadastrarPaciente extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nome=request.getParameter("nome");
        String login=request.getParameter("login");
        String senha=request.getParameter("senha");
        String sexo=request.getParameter("sexo");
        String dataNascimento=request.getParameter("data-nascimento");
        String nome_mae=request.getParameter("nomeMae");
        String uf=request.getParameter("uf");
        String cidade=request.getParameter("cidade");
        String bairro=request.getParameter("bairro");
        String endereco=request.getParameter("endereco");
        String cpf=request.getParameter("cpf");

        if(login!=null && senha !=null && nome!=null && cpf!= null && sexo!=null && dataNascimento!=null && nome_mae!=null && uf!=null && cidade!=null && bairro!=null && endereco!=null &&
                login.length()>0 && senha.length()>0 && nome.length()>0)
        {
            try {
                UsuarioDaoClasse usuarioDao = new UsuarioDaoClasse();

                boolean pacienteExiste = usuarioDao.bucarPorCPF(cpf);

                if(pacienteExiste != true){
                    Usuario u = new Usuario(0, "p", login, cpf, senha, nome, sexo, dataNascimento, nome_mae, uf, cidade, bairro, endereco);
                    try(UsuarioDaoClasse dao=new UsuarioDaoClasse())
                    {
                        dao.inserirPaciente(u);
                        //incluir CPF como param na resposta para enviar para page anamnese
                        response.sendRedirect("paciente.jsp?anamnese="+ URLEncoder.encode("sim"));
                    } catch (ErroDAO e) {
                        response.sendRedirect("paciente.jsp?mensagem="+URLEncoder.encode("Erro ao tentar cadastrar"+e));
                    } catch (Exception e) {
                        response.sendRedirect("paciente.jsp?mensagem="+URLEncoder.encode("Erro ao tentar fechar a conexão"+e));
                    }
                }else{
                    response.sendRedirect("paciente.jsp?mensagem="+URLEncoder.encode("Paciente já cadastrado!"));
                }

            } catch (ErroDAO e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
        else {
            response.sendRedirect("paciente.jsp?mensagem="+URLEncoder.encode("Está faltando dados"));
        }
    }
}
