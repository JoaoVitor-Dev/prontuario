package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Anamnese;
import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.AnamneseDaoClasse;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@WebServlet(name = "Prontuario", value = "/Prontuario")
public class Prontuario extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String codigo_request = request.getParameter("codigo");


        if(codigo_request != null){
            try {
                AnamneseDaoClasse DAOanamnese = new AnamneseDaoClasse();

                List<Anamnese> anamneseList = DAOanamnese.buscaTodasAnamenses(codigo_request);

                request.setAttribute("anamneses", anamneseList);

                request.getRequestDispatcher("/WEB-INF/Prontuario.jsp").forward(request,response);

            } catch (ErroDAO e) {
                throw new RuntimeException(e);
            }
        }else {
            response.sendRedirect("home.jsp?mensagem="+ URLEncoder.encode("Codigo nao informado"));
        }


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String data = request.getParameter("data");
        int codigo = Integer.parseInt(request.getParameter("codigo"));

        if (data != null){
            if(!Objects.equals(data, " ")){
                try {
                    AnamneseDaoClasse DAOanamnese = new AnamneseDaoClasse();

                    List<Anamnese> anamneseList = DAOanamnese.buscaAnamnesePorData(data, codigo);

                    request.setAttribute("anamneses", anamneseList);

                    request.getRequestDispatcher("/WEB-INF/Prontuario.jsp").forward(request,response);

                } catch (ErroDAO e) {
                    throw new RuntimeException(e);
                }
            }else {
                doGet(request, response);
            }
        }

        if (Objects.equals(data, "")){
            doGet(request, response);
        }

    }

}
