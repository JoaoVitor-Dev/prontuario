package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Objects;

@WebServlet(name = "logar", value = "/logar")
public class logar extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Pegar parâmetros da requisição
        String login=request.getParameter("login");
        String senha=request.getParameter("senha");

        //Abre a sessão do Usuário
        HttpSession sessao=request.getSession();
        if(sessao.getAttribute("usuario")==null)
        {
            if (login != null && senha != null &&
                    login.length() > 0 && senha.length() > 0) {
                Usuario u;

                try (UsuarioDaoClasse dao = new UsuarioDaoClasse()) {
                    u = dao.buscar(login, senha);
                    if (u != null) {
                            sessao.setAttribute("usuario", u);
                            response.sendRedirect("home.jsp");
                    } else {
                        response.sendRedirect("index.jsp?mensagem=" + URLEncoder.encode("Login ou senha incorretos")+"&"+"type=error");
                    }
                } catch (ErroDAO e) {
                    response.sendRedirect("index.jsp?mensagem=" + URLEncoder.encode("Erro ao tentar logar"));
                } catch (Exception e) {
                    response.sendRedirect("index.jsp?mensagem=" + URLEncoder.encode("Erro ao tentar fechar a conexão"));
                }
            } else {
                response.sendRedirect("index.jsp?mensagem=" + URLEncoder.encode("Está faltando dados"));
            }
        }else
        {
            response.sendRedirect("index.jsp?mensagem=" + URLEncoder.encode("Você precisa sair da sua conta antes de tentar logar com outro usuário"));
        }

    }


}
