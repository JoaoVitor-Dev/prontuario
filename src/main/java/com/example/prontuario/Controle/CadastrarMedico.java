package com.example.prontuario.Controle;

import com.example.prontuario.Modelo.Usuario;
import com.example.prontuario.Persistencia.ErroDAO;
import com.example.prontuario.Persistencia.UsuarioDaoClasse;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.net.URLEncoder;

@WebServlet(name = "CadastrarMedico", value = "/CadastrarMedico")
public class CadastrarMedico extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String nome=request.getParameter("nome");
        String cpf=request.getParameter("cpf");
        String login=request.getParameter("login");
        String senha=request.getParameter("senha");
        String especializacao=request.getParameter("especializacao");
        if(login!=null && senha !=null && nome!=null &&
                login.length()>0 && senha.length()>0 && nome.length()>0)
        {
            Usuario u = new Usuario(0,"m",nome, cpf, login, senha, especializacao );
            try(UsuarioDaoClasse dao=new UsuarioDaoClasse())
            {
                dao.inserirMedico(u);
                response.sendRedirect("medico.jsp?mensagem="+ URLEncoder.encode("Cadastrado com sucesso"));
            } catch (ErroDAO e) {
                response.sendRedirect("medico.jsp?mensagem="+URLEncoder.encode("Erro ao tentar cadastrar"));
            } catch (Exception e) {
                response.sendRedirect("medico.jsp?mensagem="+URLEncoder.encode("Erro ao tentar fechar a conexão"));
            }
        }
        else {
            response.sendRedirect("medico.jsp?mensagem="+URLEncoder.encode("Está faltando dados"));
        }
    }
}
