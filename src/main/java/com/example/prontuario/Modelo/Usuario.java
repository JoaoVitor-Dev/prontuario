package com.example.prontuario.Modelo;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Usuario {
    private int codigo;
    private String tipo, login ,senha,nome, cpf, sexo, dataNascimento, nome_mae, uf, cidade, bairro, endereco, especializacao ;
    private List<Anamnese> anamneses;


    public Usuario(){
        anamneses = new ArrayList();
    }
    public Usuario(int codigo, String tipo, String login, String cpf, String senha, String nome, String sexo, String dataNascimento, String nome_mae, String uf, String cidade, String bairro, String endereco) {
        this.codigo = codigo;
        this.tipo = tipo;
        this.login = login;
        this.senha = senha;
        this.nome = nome;
        this.sexo = sexo;
        this.dataNascimento = dataNascimento;
        this.nome_mae = nome_mae;
        this.uf = uf;
        this.cidade = cidade;
        this.bairro = bairro;
        this.endereco = endereco;
        this.cpf = cpf;
        //this.anamneses = anamneses;
    }
    public Usuario(int codigo, String tipo, String nome, String cpf,String login, String senha, String especializacao){
        this.codigo = codigo;
        this.tipo = tipo;
        this.nome = nome;
        this.cpf = cpf;
        this.login = login;
        this.senha = senha;
        this.especializacao = especializacao;

    }

    public List<Anamnese> getAnamneses() {
        return anamneses;
    }

    public void setAnamneses(List<Anamnese> anamneses) {
        this.anamneses = anamneses;
    }

    public int getCodigo() {
        return codigo;
    }
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    public String getTipo() {
        return tipo;
    }
    public String getCpf() { return cpf; }
    public void setCpf(String cpf) { this.cpf = cpf; }
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public String getDataNascimento() {
        return dataNascimento;
    }
    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
    public String getNome_mae() {
        return nome_mae;
    }
    public void setNome_mae(String nome_mae) {
        this.nome_mae = nome_mae;
    }
    public String getUf() {
        return uf;
    }
    public void setUf(String uf) {
        this.uf = uf;
    }
    public String getCidade() {
        return cidade;
    }
    public void setCidade(String cidade) {
        this.cidade = cidade;
    }
    public String getBairro() {
        return bairro;
    }
    public void setBairro(String bairro) {
        this.bairro = bairro;
    }
    public String getEndereco() {
        return endereco;
    }
    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getEspecializacao() {return especializacao;}

    public void setEspecializacao(String especializacao) {this.especializacao = especializacao;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Usuario usuario = (Usuario) o;
        return codigo == usuario.codigo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(codigo);
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "codigo=" + codigo +
                ", tipo='" + tipo + '\'' +
                ", login='" + login + '\'' +
                ", senha='" + senha + '\'' +
                ", nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", sexo='" + sexo + '\'' +
                ", dataNascimento='" + dataNascimento + '\'' +
                ", nome_mae='" + nome_mae + '\'' +
                ", uf='" + uf + '\'' +
                ", cidade='" + cidade + '\'' +
                ", bairro='" + bairro + '\'' +
                ", endereco='" + endereco + '\'' +
                ", especializacao='" + especializacao + '\'' +
                ", anamneses=" + anamneses +
                '}';
    }
}

