package com.example.prontuario.Modelo;

public class Anamnese {

    private int codigo, codigo_paciente, codigo_medico;

    String exame_fisico, laudos, hipoteses, diagnostico, tratamento, data, medico_responsavel;

    public Anamnese(int codigo, int codigo_paciente, int codigo_medico, String exame_fisico, String laudos, String hipoteses, String diagnostico, String tratamento, String data) {
        this.codigo_paciente = codigo_paciente;
        this.codigo_medico = codigo_medico;
        this.exame_fisico = exame_fisico;
        this.laudos = laudos;
        this.hipoteses = hipoteses;
        this.diagnostico = diagnostico;
        this.tratamento = tratamento;
        this.data = data;
    }

    public Anamnese() {

    }

    public String getMedico_responsavel() {
        return medico_responsavel;
    }

    public void setMedico_responsavel(String medico_responsavel) {
        this.medico_responsavel = medico_responsavel;
    }

    public int getCodigo() { return codigo; }

    public void setCodigo(int codigo) { this.codigo = codigo; }

    public int getCodigo_paciente() {
        return codigo_paciente;
    }

    public void setCodigo_paciente(int codigo_paciente) {
        this.codigo_paciente = codigo_paciente;
    }

    public int getCodigo_medico() {
        return codigo_medico;
    }

    public void setCodigo_medico(int codigo_medico) {
        this.codigo_medico = codigo_medico;
    }

    public String getExame_fisico() {
        return exame_fisico;
    }

    public void setExame_fisico(String exame_fisico) {
        this.exame_fisico = exame_fisico;
    }

    public String getLaudos() {
        return laudos;
    }

    public void setLaudos(String laudos) {
        this.laudos = laudos;
    }

    public String getHipoteses() {
        return hipoteses;
    }

    public void setHipoteses(String hipoteses) {
        this.hipoteses = hipoteses;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }

    public String getTratamento() {
        return tratamento;
    }

    public void setTratamento(String tratamento) {
        this.tratamento = tratamento;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }



    @Override
    public String toString() {
        return "Anamnese{" +
                ", codigo_paciente=" + codigo_paciente +
                ", codigo_medico=" + codigo_medico +
                ", exame_fisico='" + exame_fisico + '\'' +
                ", laudos='" + laudos + '\'' +
                ", hipoteses='" + hipoteses + '\'' +
                ", diagnostico='" + diagnostico + '\'' +
                ", tratamento='" + tratamento + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
