<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 05/06/2023
  Time: 22:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title>Meus Dados</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
  <span><a href="home.jsp">Home</a></span><span>Meus Dados</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>
<main>
    <c:choose>
        <c:when test="${sessionScope.usuario.tipo == 'p'}">
            <form action="EditarPaciente" method="POST" class="formulario-usuario">
                <span>DADOS PESSOAIS</span>
                    <br>
                        <hr>
                <br>
                <p>Nome completo: <c:out value="${sessionScope.usuario.nome}"></c:out> </p>
                <br>
                <p>CPF: <c:out value="${sessionScope.usuario.cpf}"></c:out></p>
                <br>
                <p>Login: <c:out value="${sessionScope.usuario.login}"></c:out></p>

                <label for="endereco">Endereço:</label>
                <input type="text" id="endereco" name="endereco" value="<c:out value="${sessionScope.usuario.endereco}"></c:out>" required>
                <br><br>
                <label for="senha">Senha:</label>
                <input type="password" id="senha" name="senha" value="<c:out value="${sessionScope.usuario.senha}"></c:out>" required>
                <br><br>
                <button class="button">Atualizar</button>
             </form>
            </c:when>
        </c:choose>

        <c:choose>
            <c:when test="${sessionScope.usuario.tipo == 'm'}">
                <form action="EditarMedico" method="POST" class="formulario-usuario">
                <label for="nome">Nome Completo:</label>
                <input type="text" id="nome" name="nome" value="<c:out value="${sessionScope.usuario.nome}"></c:out>" required>
                <label>CPF: </label>
                <input type="text" maxlength="11" name="cpf" value="<c:out value="${sessionScope.usuario.cpf}"></c:out>" required style="height: 25px">
                <br>
                <label for="senha">Senha:</label>
                <input type="password" id="senha" name="senha" value="<c:out value="${sessionScope.usuario.senha}"></c:out>" required>
                <br><br>
                <span>ESPECIALIZAÇÃO</span>
                <hr>
                <br>
                <label for="nome">Especialização:</label>
                <br>
                    <textarea id="especializacao" name="especializacao" value="<c:out value="${sessionScope.usuario.especializacao}"></c:out>"><c:out value="${sessionScope.usuario.especializacao}"></c:out></textarea>
                <br><br>
                    <button class="button">Atualizar</button>
                </form>
            </c:when>
        </c:choose>
</main>

<c:import url="WEB-INF/Mensagem.jsp"/>

<footer class="footer">
</footer>
</body>
</html>
