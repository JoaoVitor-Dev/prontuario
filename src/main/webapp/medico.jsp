<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 05/06/2023
  Time: 22:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html

        lang="pt-br">
<head>
    <meta charset="UTF-8">
    <title>Médico</title>
  <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
  <span><a href="home.jsp">Home</a></span><span>Cadastrar Médicos</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>
<main>
    <form action="CadastrarMedico" method="post" class="formulario-usuario">

        <span>DADOS PESSOAIS</span>
        <hr>

        <label for="nome">Nome Completo:</label>
        <input type="text" id="nome" name="nome" required>

        <label>CPF: </label>
        <input type="text" maxlength="11" name="cpf" style="height: 25px">

        <br>
        <label for="login">Login:</label>
        <input type="text" id="login" name="login" required>
        <br>
        <br>
        <label for="senha">Senha:</label>
        <input type="password" id="senha" name="senha" required>
        <br><br>
        <span>ESPECIALIZAÇÃO</span>
        <hr>
        <br>
        <label for="nome">Especialização:</label>
        <br>
        <textarea id="especializacao" name="especializacao" required></textarea>
        <br><br>
        <input class="button" type="submit" value="Cadastrar">
    </form>
</main>
<c:import url="WEB-INF/Mensagem.jsp"/>

<c:if test="${not empty param.tipo}">${param.tipo}</c:if>
<footer class="footer">
</footer>
</body>
</html>
