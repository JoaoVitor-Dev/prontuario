<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 05/06/2023
  Time: 22:09
  To change this template use File | Settings | File Templates.
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pacientes</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
  <span><a href="home.jsp">Home</a></span><span>Cadastrar Pacientes</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>
<main>
    <form action="CadastrarPaciente" method="post" class="formulario-usuario">

        <span>DADOS PESSOAIS</span>
        <hr>

        <label for="nome">Nome Completo:</label>
        <input type="text" id="nome" name="nome" required style="width: 460px">

        <label>CPF: </label>
        <input type="text" maxlength="11" name="cpf" style="height: 25px">


        <br>
        <label for="login">Login:</label>
        <input type="text" id="login" name="login" required>
        <br><br>
        <label for="senha">Senha:</label>
        <input type="password" id="senha" name="senha" required>
        <br><br>

        <label>Sexo:
            <label for="m">Masculino:<input type="radio" id="m" name="sexo" value="M" required style="width: 20px"> </label>
            <label for="f">Feminino<input type="radio" id="f" name="sexo" value="F" required style="width: 20px"></label>
        </label>

        <label for="data-nascimento">Data de Nascimento:</label>
        <input type="date" id="data-nascimento" name="data-nascimento" required>

        <br><br>
        <span>FILIAÇÃO</span>
        <hr>
        <label for="nome-mae">Nome da Mãe:</label>
        <input type="text" id="nome-mae" name="nomeMae" required style="width: 460px">

        <br><br>
        <span>ENDEREÇO</span>
        <hr>

        <label for="uf">UF:</label>
        <input type="text" id="uf" name="uf" maxlength="2" required style="width: 52px; text-transform: uppercase">

        <label for="cidade">Cidade:</label>
        <input type="text" id="cidade" name="cidade" required>
        <br>
        <label for="bairro">Bairro:</label>
        <input type="text" id="bairro" name="bairro" required>
        <br>
        <label for="endereco">Endereço:</label>
        <input type="text" id="endereco" name="endereco" required>
        <br><br>
        <input class="button" type="submit" value="Cadastrar">
    </form>
</main>

<c:if test="${param.anamnese == 'sim'}">
    <div class="escopomensagem">
            <p>Paciente cadastrado com sucesso!</p>
        <a href="anamnese.jsp"><button class="button">Anamnese</button></a>
    </div>
</c:if>

<c:import url="WEB-INF/Mensagem.jsp"/>

<footer class="footer">
</footer>
</body>
</html>
