<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
    <span></span><p class="title-login"><i>Sistema de Prontuário Médico</i></p><span></span>
</header>
<main class="container">

    <form action="logar" method="post" class="box border">

        <label for="login">Login:</label>
        <input class="" type="text" name="login" id="login" autofocus style="height: 25px">
        <label for="login">Senha:</label>
        <input class="" type="password" name="senha" id="senha" style="height: 25px">
        <button class="button">Acessar</button>
    </form>
</main>

<c:import url="WEB-INF/Mensagem.jsp"/>

<footer class="footer">
</footer>
</body>
</html>