<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 18/06/2023
  Time: 01:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title>Meu Prontuário</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
    <span><a href="home.jsp">Home</a></span><span>Prontuário Médico</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>

<main>
    <div class="filtro-data">
        <form action="Prontuario" method="post">
            <label for="data">Data: </label>
            <input type="date" name="data" id="data" required>
            <input type="text" class="none" name="codigo" value="${param.codigo}">
            <button class="button">Filtrar data</button>
        </form>
            <a href="Prontuario?codigo=${param.codigo}"><button class="button">Todos</button></a>
    </div>

    <div class="lista-anamenses">
        <article>
            <c:forEach var="anamneses" items="${anamneses}">
                <section>
                    <p>Exame Físico: ${anamneses.exame_fisico}</p>
                    <p>Laudos: ${anamneses.laudos}</p>
                    <p>Hipoteses: ${anamneses.hipoteses}</p>
                    <p>Diagnostico: ${anamneses.diagnostico} </p>
                    <p>Tratamento: ${anamneses.tratamento}</p>
                    <p>Data: ${anamneses.data}</p>
                    <p>Médico responsável: ${anamneses.medico_responsavel}</p>
                </section>
            </c:forEach>
        </article>
    </div>
</main>

<footer class="footer">
</footer>
</body>
</html>
