<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 20/06/2023
  Time: 12:45
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edita pacienteTitle</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
    <span><a href="home.jsp">Home</a></span><span>Editar Paciente</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>

<main>
    <form action="EditarPacientePorMedico" method="post" class="formulario-usuario">
        <span>DADOS PESSOAIS</span>
        <hr>
        <label for="nome">Nome Completo:</label>
        <input type="text" id="nome" name="nome" value="${paciente.nome}" style="width: 460px">

        <label>CPF: </label>
        <input type="text" maxlength="11" name="cpf" value="${paciente.cpf}" style="height: 25px">

        <input type="text" class="none" name="codigo" value="${paciente.codigo}">

        <br>
        <label for="login">Login:</label>
        <input type="text" id="login" name="login" value="${paciente.login}">
        <br><br>
        <label for="senha">Senha:</label>
        <input type="password" id="senha" name="senha" value="${paciente.senha}">
        <br><br>

        <br><br>
        <span>FILIAÇÃO</span>
        <hr>
        <label for="nome-mae">Nome da Mãe:</label>
        <input type="text" id="nome-mae" name="nomeMae" value="${paciente.nome_mae}" style="width: 460px">

        <br><br>
        <span>ENDEREÇO</span>
        <hr>

        <label for="uf">UF:</label>
        <input type="text" id="uf" name="uf" maxlength="2" value="${paciente.uf}" required style="width: 52px; text-transform: uppercase">

        <label for="cidade">Cidade:</label>
        <input type="text" id="cidade" name="cidade" value="${paciente.cidade}" required>
        <br>
        <label for="bairro">Bairro:</label>
        <input type="text" id="bairro" name="bairro" value="${paciente.bairro}" required>
        <br>
        <label for="endereco">Endereço:</label>
        <input type="text" id="endereco" name="endereco" value="${paciente.endereco}" required>
        <br><br>
        <button class="button">Atualizar dados</button>
    </form>
</main>
</body>
</html>
