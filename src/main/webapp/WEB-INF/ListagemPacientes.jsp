<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 19/06/2023
  Time: 21:56
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pacientes</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
    <span><a href="home.jsp">Home</a></span><span>Listagem de Pacientes</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>

<h4 style="text-align: center; margin-top: 25px">Médico: <c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.nome}</c:if></h4>

<main>
    <div class="filtro-data">
        <form action="ListarPacientes" method="get">
            <label for="paciente">Paciente: </label>
            <input type="text" name="nome" id="paciente" required>
            <button class="button">Pesquisar</button>
        </form>
        <a href="paciente.jsp" class="novo">Novo</a>
    </div>

    <div class="lista-anamenses">
        <article>
            <c:forEach var="pacientes" items="${pacientes}">
                <section>
                    <p>Nome: ${pacientes.nome} | CPF: ${pacientes.cpf} | Data de Nascimento: ${pacientes.dataNascimento} </p>
                    <br>
                    <a href="Prontuario?codigo=${pacientes.codigo}">
                        <button class="button">Prontuário</button>
                    </a>
                    <a href="anamnese.jsp?cpf=${pacientes.cpf}">
                        <button class="button">Anamnese</button>
                    </a>
                    <a href="EditarPacientePorMedico?codigo=${pacientes.codigo}">
                        <button class="button">Editar dados</button>
                    </a>
                </section>
            </c:forEach>
        </article>
    </div>
</main>
<footer class="footer">
</footer>

</body>
</html>
