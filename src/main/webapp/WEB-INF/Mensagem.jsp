<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 21/06/2023
  Time: 12:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>

<c:if test="${not empty param.mensagem}">
        <div class="pop-up">
                ${param.mensagem}
        </div>
</c:if>
