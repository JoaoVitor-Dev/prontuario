<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 19/06/2023
  Time: 21:28
  To change this template use File | Settings | File Templates.
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Pacientes</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header class="navbar">
    <span><a href="home.jsp">Home</a></span><span>Pesquisar Pacientes</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>

<h4 style="text-align: center; margin-top: 25px">Médico: <c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.nome}</c:if></h4>

<main>
    <div class="filtro-data">
        <form action="ListarPacientes" method="get">
            <label for="paciente">Paciente: </label>
            <input  type="text" name="nome" id="paciente">
            <button class="button">Pesquisar</button>
        </form>
        <a href="paciente.jsp" class="novo">Novo</a>
    </div>
</main>

<footer class="footer">
</footer>
</body>
</html>
