<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 04/06/2023
  Time: 19:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title>Anamnese</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
    <span><a href="home.jsp">Home</a></span><span>Cadastrar Anamenese</span><span><c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a href="Sair">Sair</a></span>
</header>
<main>
    <form class="formulario-anamnese" action="CadastrarAnamnese" method="post">
        <label>
            Informe o CPF do Paciente *:</label>
            <input type="text" maxlength="11" name="cpf" style="height: 25px" value="${param.cpf}">

        <label for="exame-fisico">Exame Físico:</label>
        <textarea id="exame-fisico" name="exame-fisico" required></textarea>

        <label for="exames-complementares">Exames Complementares:</label>
        <textarea id="exames-complementares" name="exames-complementares" required></textarea>

        <label for="laudos">Laudo:</label>
        <textarea id="laudos" name="laudos" required></textarea>

        <label for="hipoteses-diagnosticas">Hipóteses Diagnósticas:</label>
        <textarea id="hipoteses-diagnosticas" name="hipoteses-diagnosticas" required></textarea>

        <label for="diagnostico-definitivo">Diagnóstico Definitivo:</label>
        <textarea id="diagnostico-definitivo" name="diagnostico-definitivo" required></textarea>

        <label for="tratamento-efetuado">Tratamento Efetuado:</label>
        <textarea id="tratamento-efetuado" name="tratamento-efetuado" required></textarea>

        <label for="data-hora">Data e Hora:</label>
        <input type="date" id="data-hora" name="data-hora" required>

        <input class="none" type="text" id="medico-responsavel" name="medico-responsavel" value=<c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.codigo}</c:if>>

        <input class="button" type="submit" value="Enviar">
    </form>
</main>

<c:if test="${not empty param.mensagem}">
    <div class="escopomensagem">
            ${param.mensagem}
    </div>
</c:if>

<footer class="footer">
</footer>
</body>
</html>
