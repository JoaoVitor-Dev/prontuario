<%--
  Created by IntelliJ IDEA.
  User: JoaoV
  Date: 30/05/2023
  Time: 20:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
<head>
    <title>Home</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<header class="navbar">
    <span></span><span>Sistema de Prontuário Eletrônico</span> <span>Bem vindo <c:if test="${not empty sessionScope.usuario}">${sessionScope.usuario.login}</c:if> | <a class="sair" href="Sair">Sair</a></span>
</header>

<main class="container">
    <div class="container">
        <div class="menu">
            <c:if test="${sessionScope.usuario.tipo == 'm'}">
                <a class="atendimento" href="pesquisapacientes.jsp" autofocus>Novo Atendimento</a>
                <a class="pacientes" href="paciente.jsp">Cadastrar Paciente</a>
                <a class="medicos" href="medico.jsp">Cadastrar Médico</a>
                <a class="prontuario" href="pesquisapacientes.jsp">Prontuários</a>
            </c:if>

            <c:if test="${sessionScope.usuario.tipo == 'p'}">
                <a class="prontuario" href="Prontuario?codigo=${sessionScope.usuario.codigo}">Meu Prontuário</a>
            </c:if>

            <a class="dados" href="dados.jsp">Meus Dados</a>
        </div>
    </div>
</main>

<c:import url="WEB-INF/Mensagem.jsp"/>

<footer class="footer">
</footer>
</body>
</html>
